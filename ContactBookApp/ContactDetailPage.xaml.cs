﻿using System;
using ContactBookApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ContactBookApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactDetailPage : ContentPage
    {
        public event EventHandler<Contact> ContactAdded;
        public event EventHandler<Contact> ContactUpdated;

        public static readonly BindableProperty ContactProperty =
            BindableProperty.Create("EntryCell", typeof(Contact), typeof(ContactDetailPage));

        public Contact MyContact
        {
            get => (Contact) GetValue(ContactProperty);
            set => SetValue(ContactProperty, value);
        }

        public ContactDetailPage(Contact contact)
        {
            MyContact = contact;
            BindingContext = this;

            InitializeComponent();
        }

        async void OnSave(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(MyContact.FirstName) ||
                String.IsNullOrWhiteSpace(MyContact.LastName)) 
            {
                await DisplayAlert("Error", "Please enter the name.", "OK");
                return;
            }
            if (MyContact.Id == -1)
            {
                MyContact.Id = new Random().Next();
                ContactAdded?.Invoke(this, MyContact);
            }
            else
                ContactUpdated?.Invoke(this, MyContact);

            await Navigation.PopAsync();
        }
    }
}