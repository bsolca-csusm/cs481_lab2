﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactBookApp.Models;
using ContactBookApp.Services;
using Xamarin.Forms;

namespace ContactBookApp
{
    public partial class MainPage : ContentPage
    {
        private ContactService _service;
        private ObservableCollection<Contact> _contacts;
        public MainPage()
        {
            _service = new ContactService();
            _contacts = _service.GetContacts();
            InitializeComponent();

            ListView.ItemsSource = _contacts;
        }

        async void OnAddContact(object sender, EventArgs e)
        {
            Contact myContact = new Contact();
            myContact.Id = -1;
            var page = new ContactDetailPage(myContact);

            page.ContactAdded += (source, contact) =>
            {
                _service.AddContact(contact);
            };

            await Navigation.PushAsync(page);
        }

        async void OnContactSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (ListView.SelectedItem == null)
                return; 

            var selectedContact = e.SelectedItem as Contact;
            ListView.SelectedItem = null;

            var page = new ContactDetailPage(selectedContact);
            page.ContactUpdated += (source, contact) =>
            {
                selectedContact.Id = contact.Id; 
                selectedContact.FirstName = contact.FirstName;
                selectedContact.LastName = contact.LastName;
                selectedContact.Phone = contact.Phone;
                selectedContact.Email = contact.Email;
                selectedContact.IsBlocked = contact.IsBlocked;
            };
            await Navigation.PushAsync(page);
        }

        private void DeleteContact(object sender, EventArgs e)
        {
            var menuItem = sender as MenuItem;
            var contact = menuItem.CommandParameter as Contact;
            _service.DeleteContact(contact.Id);
            _contacts = _service.GetContacts();
        }
    }
}
